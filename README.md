# Cert-Manager ClouDNS DNS01 Provider

A Cert-Manager DNS01 provider for ClouDNS.

## Note

This repo has a few changes in order to create `x86_64` and `arm64` images, plus some template and version updates.

## Configuration

Cert-Manager expects DNS01 providers to parse configuration from incoming webhook requests.

This can be used to have multiple Cert-Manager `Issuer` resources use the same instance of the provider with different credentials or configuration.

Because we currently don't need this and the LEGO library already has support for parsing environment variables (and files), we have opted to not use this.

The `testdata/config.json` file is there because the DNS01 provider conformance testing suite wants to mock the requests away, and needs a folder to load the data from.

### Environment Options

|Name|Required|Description|
|---|---|---|
|`GROUP_NAME`|yes|Used to organise cert-manager providers, this is usually a domain|
|`CLOUDNS_AUTH_ID_FILE`|yes|Path to file which contains ClouDNS Auth ID|
|`CLOUDNS_AUTH_ID_TYPE`| no, default: auth-id | change to `sub-auth-id` to use a sub-user (created via [Reseller](https://www.cloudns.net/api-settings/)) |
|`CLOUDNS_AUTH_PASSWORD_FILE`|yes|Path to file which contains ClouDNS Auth password|
|`CLOUDNS_TTL`|no, default: 60|ClouDNS TTL|
|`CLOUDNS_HTTP_TIMEOUT`|no, default: 30 seconds|ClouDNS API request timeout|

## Install

Install the webhook the following way.

    git clone https://gitlab.com/mschirrmeister/cert-manager-webhook-cloudns.git
    cd cert-manager-webhook-cloudns

    make rendered-manifest.yaml
    kubectl apply -f .out/rendered-manifest.yaml

or

    helm install cert-manager-webhook-cloudns deploy/cert-manager-webhook-cloudns

### Create secret for webook

    kubectl create secret generic cert-manager-webhook-cloudns-api-secret \
    	--from-file .creds/auth_id \
    	--from-file .creds/auth_password

Or create the secret via a manifest. Create a file `cert-manager-webhook-cloudns-api-secret.yaml` with the following content and apply it.

    apiVersion: v1
    kind: Secret
    type: Opaque
    metadata:
      name: cert-manager-webhook-cloudns-api-secret
    stringData:
      auth_id: <auth_id>
      auth_password: <auth_password>

    kubectl apply -f cert-manager-webhook-cloudns-api-secret.yaml
  
## Development

### Running DNS01 provider conformance testing suite

```bash
# Get kubebuilder
./scripts/fetch-test-binaries.sh

# Run testing suite
TEST_ZONE_NAME=<domain> CLOUDNS_AUTH_ID_FILE=.creds/auth_id CLOUDNS_AUTH_PASSWORD_FILE=.creds/auth_password CLOUDNS_AUTH_ID_TYPE=sub-auth-id make verify

# Cleanup after testing (esp. needed when tests have failed)
remove `~/.cache/kubebuilder-envtest/*`
```

